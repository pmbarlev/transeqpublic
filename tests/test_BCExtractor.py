from unittest import TestCase
from nose.plugins.attrib import attr
from nose.tools import nottest, istest
from transeq.ExtractBCSR import BCExtractor
from os import path
import tempfile
import filecmp


@nottest
class TestBCExtractorFinalOutBase(TestCase):
    def setUp(self):
        self.input_dir = "../test_data/ExtractBC/input"
        self.out_dir = tempfile.mkdtemp()
        self.expected_out_dir = "../test_data/ExtractBC/expected_out"
        self.fastq_in = path.join(self.input_dir, "4reads.R1.fastq")
        self.fastq_umi = path.join(self.input_dir, "4reads.R2.fastq")
        self.bc_regex = "(^[A-Za-z]{6})"
        self.fastq_out = path.join(self.out_dir, "4reads.bcExt.R1.fastq")
        self.bc_hist = path.join(self.out_dir, "4readHist.csv")
        self.fastq_out_exp = path.join(self.expected_out_dir, "4reads.bcExt.R1.fastq")
        self.hist_exp = path.join(self.expected_out_dir, "4readHist.csv")

    # Test with a UMI fastq
    def test_final_out(self):
        # extract the barcodes
        self.bc_extractor()
        self.bc_extractor.write_hist(self.bc_hist)
        # Compare output files
        res = True
        msg = ""
        if not filecmp.cmp(self.fastq_out, self.fastq_out_exp, shallow=False):
            res = False
            msg = "fastq_out file differs from expected."
        if not filecmp.cmp(self.bc_hist, self.hist_exp, shallow=False):
            res = False
            msg = "fastq_out file differs from expected."
        self.assertTrue(res, msg=msg)


# Test when UMI's are in a separate fastq file
@istest
@attr(test_type='unit')
class TestBCExtractor4readsUmiWithUmiFastq(TestBCExtractorFinalOutBase):
    def setUp(self):
        super(TestBCExtractor4readsUmiWithUmiFastq, self).setUp()
        self.bc_extractor = BCExtractor(self.fastq_in, self.fastq_out, self.bc_regex, umi_fastq_name=self.fastq_umi)


# Test when UMI's are in the Read 1 fastq
@istest
@attr(test_type='unit')
class TestBCExtractor4readsUmiNoUmiFastq(TestBCExtractorFinalOutBase):
    def setUp(self):
        super(TestBCExtractor4readsUmiNoUmiFastq, self).setUp()
        self.fastq_out_exp = path.join(self.expected_out_dir, "4reads.bcExt_read1.R1.fastq")
        self.hist_exp = path.join(self.expected_out_dir, "4readHist_read1.csv")
        self.bc_extractor = BCExtractor(self.fastq_in, self.fastq_out, self.bc_regex)


# Test when bc_regex is an int
@istest
@attr(test_type='unit')
class TestBCExtractor4readsBcRegexInt(TestBCExtractorFinalOutBase):
    def setUp(self):
        super(TestBCExtractor4readsBcRegexInt, self).setUp()
        self.bc_regex = "6"
        self.bc_extractor = BCExtractor(self.fastq_in, self.fastq_out, self.bc_regex, umi_fastq_name=self.fastq_umi)



