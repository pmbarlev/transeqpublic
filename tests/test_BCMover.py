from unittest import TestCase
from nose.tools import nottest, istest
from nose.plugins.attrib import attr
from transeq.MoveBC import BCMover
from os import path
import tempfile
import filecmp

@nottest
class TestBCMoverBase(TestCase):
    def setUp(self):
        self.input_dir = "../test_data/ExtractBC/input"
        self.out_dir = tempfile.mkdtemp()
        self.expected_out_dir = "../test_data/ExtractBC/expected_out"


    def test_final_out(self):
        self.bc_mover = BCMover(self.bam_in, self.bam_out)
        # extract the barcodes
        self.bc_mover()
        # Compare output bam
        res = True
        msg = ""
        if not filecmp.cmp(self.bam_out, self.bam_out_exp, shallow=False):
            res = False
            msg = "bam_out file differs from expected."
        self.assertTrue(res, msg=msg)

@istest
@attr(test_type='unit')
class TestBCMoverSmall(TestBCMoverBase):
    def setUp(self):
        super(TestBCMoverSmall, self).setUp()
        self.bam_in = path.join(self.input_dir, "small.bam")
        self.bam_out = path.join(self.out_dir, "small.moveBC.bam")
        self.bam_out_exp = path.join(self.expected_out_dir, "small.moveBC.bam")

