from unittest import TestCase
from os import path
import tempfile
import filecmp
from transeq.MarkDuplicatesUMIbyGene import MarkUmiDuplicates


class TestMarkUmiDuplicates(TestCase):
    def setUp(self):
        self.input_dir = "../test_data/MarkDuplicates/input"
        self.expected_out_dir = "../test_data/MarkDuplicates/expected_out"
        self.out_dir = tempfile.mkdtemp()
        self.gtf= path.join(self.input_dir, "genes.gtf")
        self.bam_in = path.join(self.input_dir, "highCount_small.bam")
        self.bam_out = path.join(self.out_dir, "highCount_small.MD.bam")
        self.bc_length = 8
        self.out_prefix = "highCount_small"


    def test_final_out(self):
        dup_marker = MarkUmiDuplicates(self.bam_in, self.bam_out, self.bc_length, gtf=self.gtf)
        dup_marker()
        dup_marker.write_metrics(self.out_dir, self.out_prefix)

        #expected outputs
        self.bam_out_exp = path.join(self.expected_out_dir, "highCount_small.MD.bam")
        self.dup_hist_exp = path.join(self.expected_out_dir, "highCount_small.dupHistogram.csv")
        self.metrics_exp = path.join(self.expected_out_dir, "highCount_small.metrics.csv")
        # Compare output
        res = True
        msg = ""
        if not filecmp.cmp(self.bam_out, self.bam_out_exp, shallow=False):
            res = False
            msg = "bam_out file differs from expected. "
        if not filecmp.cmp(self.dup_hist_exp,
                           path.join(self.out_dir, "highCount_small.dupHistogram.csv"), shallow=False):
            res = False
            msg += "dup_histogram file differs from expected. "
        if not filecmp.cmp(self.metrics_exp,
                           path.join(self.out_dir, "highCount_small.metrics.csv"), shallow=False):
            res = False
            msg += "metrics file differs from expected. "
        self.assertTrue(res, msg=msg)
