from unittest import TestCase
from transeq.CorrectCounts import *
import tempfile
import filecmp

class TestCorrect_counts(TestCase):
    def setUp(self):
        self.count_in = "../test_data/CorrectCounts/input/highCount.count.MDgene.txt"
        self.count_out_exp = "../test_data/CorrectCounts/expected_out/highCount.count.MDgene.corrected.txt"
        self.count_out = tempfile.mktemp()

    def test_final_out(self):
        counts = correct_counts(self.count_in, 8)
        write_counts_file(counts, self.count_out)
        res = True
        msg = ""
        if not filecmp.cmp(self.count_out, self.count_out_exp, shallow=False):
            res = False
            msg = "count file differs from expected."
        self.assertTrue(res, msg=msg)


