Trans/Mars-seq feature counting protocol
========================================

Overview
========

Custom components for processing data from RNA sequencing experiments prepared using the Trans/Mars-seq protocol:

1. ExtractBC.py: Pre-mapping processing of fastq files in order to extract UMI barcodes from the read sequences to the fastq-read ID line.

2. MoveBC.py: post-mapping processing of the bam files in order to move the UMI barcodes from the query names to bam tags.

3. CorrectCouts.py: Correcting read counts to account for barcode clashing. 

## Custom script help and details

All custom componenets are python scripts. Type `<script>.py --help` for a list of required and optional parameters.

Pre-requisite installed tools
------------------------------
1. Python 2.7
2. Java 8 (needed for Picard MarkDuplicates v2.2)
2. Samtools >= 1.2
3. STAR >= 2.5 (or your favorite mapper)
4. Picard >= 2.2
4. These custom scripts

Processing workflow
-------------------

1. Run ExtractBC.py to create new fastq files with the UMI barcodes extracted.
2. Remove poly A/T reads (and cut starting bases, if required) using cutadapt. 
2. Map the fastq from step 2 using any mapper (e.g., Bowtie or STAR).
3. Run MoveBC.py on the result of step 3.
3. Sort and index the resulting bam using 'samtools sort' and 'samtools index'.
4. HTSeq count, raw counts.
4. Mark duplicates.
5. HTseq count, de-duplicated.
6. Count correction (mars-seq only).

Remark: Many of these steps can be piped, which will significantly reduce I/O and speed up the process.  


Example
=======

## Run ExtractBC
```
cd examples/highCount

fastq1=highCount_R1.fastq.gz
fastq2=highCount_R2.fastq.gz
sample=highCount
fastq1_ExtractBC=expected_out/highCount_R1.bcExt.fastq

ExtractBCSR=/path/to/TranseqPublic/transeq/ExtractBCSR.py

python $ExtractBCSR \
--fastqIn $fastq1 \
--fastqUMI $fastq2 \
--fastqOut $fastq1_ExtractBC \
--bcRegex 8 > expected_out/highCount.ExtractBC.log 2>&1
```

## Poly A/T read removal
The following application of `cutadapt` will remove the first 3 bases of each read,  the universal Illumina adapter, and poly A/T sequences.
```
fastq1_cutadapt=expected_out/highCount_R1.bcExt.cutadapt.fastq
cutadaptLog=expected_out/cutadapt.log

cutadapt \
-a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC \
-a "A{100}" -a "T{100}" --times 2 \
--cut 3 \
-o $fastq1_cutadapt \
-m 40 \
$fastq1_ExtractBC \
> $cutadaptLog 2>&1
```

## Map using STAR
```
genomeDir=/path/to/Mus_musculus/GRCm38/sequence/STAR_index
gtf=/path/to/Mus_musculus.GRCm38.82.gtf
STAR=/path/to/STAR/bin/Linux_x86_64/STAR
outPrefix=expected_out/STAR/highCount. ; mkdir -p expected_out/STAR

$STAR --genomeDir $genomeDir \
--runThreadN 4 \
--readFilesIn $fastq1_cutadapt \
--sjdbGTFfile $gtf \
--outFileNamePrefix $outPrefix \
--outSAMtype BAM SortedByCoordinate \
--outSAMunmapped Within
```

## MoveBC 
```
bamIn=$outPrefix'Aligned.sortedByCoord.out.bam'
bamBC=expected_out/highCount.bam
MoveBC=/path/to/TranseqPublic/transeq/MoveBC.py

python $MoveBC \
--bamIn $bamIn \
--bamOut $bamBC
```

## Add feature tag to bam, and count raw reads (using HTSeq count)


```
bamGene=expected_out/highCount.gene.bam
# Save sam header (htseq is barbaric, and doesn't output the header...)
header=expected_out/header.sam
samtools view -H $bamBC > $header

samtools view -h $bamBC | \
htseq-count  \
-i gene_name \
-o /dev/fd/3 \
- $gtf \
3>&1 >expected_out/highCount_count.txt | \
cat $header - | samtools view -Sb - > $bamGene
```

Remark: STAR can almost perform this step during mapping (it can already count reads on features), but currently does not include the option of inserting a feature bam tag.


## Index Bam
```
samtools index $bamGene
```

## Marking duplicate reads (setting the duplicate-read flag in the bam)

Here there is a difference between mars-seq and trans-seq. In trans-seq, UMI-barcodes are added post fragmetation, so duplicate reads will have the same UMI-barcode and mapped position. In contrast, in the mars-seq protocol UMI-barcodes are added pre-fragmentation, followed by some PCR amplification, and only then are the molecules fragmented. Consequently, duplicate reads will have the same UMI-barcode, but may map differently (though they will map to the same gene). Consequently, we give two variations on the workflow. In either case, the output of this step is a bam in which the duplicate read bam flag has been set.

1. Marking duplicates based on locus level position (for Trans-seq)
    In this case, read counts at each position are typically << number of barcodes (4^barcode length), and thus barcode clashing is minimal, so no correction is used. 
    
        ```
        bamMD=expected_out/highCount.gene.MD.bam
        
        java -jar picard.jar MarkDuplicates \
        INPUT=$bamGene \
        OUTPUT=$bamMD \
        BARCODE_TAG=RX \
        METRICS_FILE=markduplicates.metrics.txt
        ```
    

2. Marking duplicates based on gene level position (for Mars-seq)

    This step uses a custom script. 
    
        ```
        MarkDuplicates=/path/to/TranseqPublic/transeq/MarkDuplicatesUMIbyGene.py
        gtf=/bio/db/genomes/Mus_musculus/GRCm38/annotation/Ensembl/82/Mus_musculus.GRCm38.82.gtf
        bamMD=expected_out/highCount.gene.MD.bam
        deDupMetricsDir=expected_out/deDupMetrics; mkdir -p $deDupMetricsDir
        
        python $MarkDuplicates \
        --inputBam $bamGene \
        --outputBam $bamMD \
        --bcLength 8 \
        --gtf $gtf \
        --outputMetricsDir $deDupMetricsDir \
        --outputPrefix $sample \
        --logFile expected_out/highCount.deDup.log
        ```
    
    Remarks: 
         
    1. In order to reduce runtime, it is possible to generate a restricted gtf with only `gene` feature entries (e.g., using `awk '$3=="gene"' $gtf > genes.gtf`), and to use this gtf with `MarkDuplicatesUMI.py`.
    2. There is an option (`numMismatches`) for correcting sequencing errors in the UMI-barcodes, implementing the adjacency graph algorithm described in https://cgatoxford.wordpress.com/2015/08/14/unique-molecular-identifiers-the-problem-the-solution-and-the-proof/. However, use of this option is currently not recommended. 
        
## Index `*.MD.bam`

```
samtools index $bamMD
```

## De-duplicated counts

Rerun htseq-count on deDup bams. 

```
UMIcounts=expected_out/highCount.deDup_count.txt  

samtools view -F 1024 $bamMD | \
htseq-count \
-i gene_name \
- $gtf \
2>&1 > $UMIcounts
```

### Barcode clashing correction

*Note:* This step should **ONLY** be performed when de-Duplication is performed by UMI-barcode+gene (using the custom script).

UMI barcodes are assigned to fragments (semi-) randomly, so it might happen that two independent fragments get assigned the same UMI barcode. When the number of fragments mapping to a gene are low, there are few such clashes, and the effect on the counts is negligible. However, when the counts are of the same order of magnitude as the number of barcode options ( 4<sup>bcLength</sup> ) clashing becomes significant, and a correction should be incorporated. More details concerning this correction can be found in `occupancy.pdf`.

```
CorrectCountsScript=/path/to/TranseqPublic/transeq/CorrectCounts.py
UMIcountsCorrected=expected_out/highCount.deDup_count.corrected.txt 

python  $CorrectCountsScript \
--inputCounts $UMIcounts \
--bcLength 8 \
--outputCounts $UMIcountsCorrected
```